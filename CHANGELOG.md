# Changelog

## Version 15.0.0 (2024-09-09)

### Changes

- OS changed from Centos7 to Alma9
- All packages upgraded
- $CHEOPS_SW now points to /opt/cheops
- Enable cheops dnf repository
- Environment of cheops user moved from ~/.bashrc to ~/.bashrc.d/bashrc_cheops
- New python packages: pyarrow, pypdf
- New system packages: ant, graphviz, tomcat

## Version 14.0.0 (2024-02-14)

_First release._
