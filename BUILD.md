# How to build the Docker image

## Build locally

Run the following command to build a local version of the image:

    docker build --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t <name>:<version> .

where `<name>:<version>` is to be replaced with the name and version of your choice.

## Build and push image to gitlab 

Run the following commands to build and push a multi-architecture version of the image to the gitlab container registry (requires sufficient permissions in gitlab).

    docker login registry.gitlab.unige.ch

    docker buildx build --platform linux/arm64,linux/amd64 --build-arg BUILD_DATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ') -t registry.gitlab.unige.ch/cheops/containers/cheops-dev:<x.y.x> --push --progress=plain . > build.log 2>&1

where `<x.y.z>` is to be replaced with the version of the image.
