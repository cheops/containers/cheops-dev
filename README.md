# CHEOPS development container for Visual Studio Code

The source code editor Visual Studio Code can, with the help of the Dev Container plugin, use prebuilt Docker containers as the runtime development environment.

The `cheops-dev` container was created for this purpose. The container comes preinstalled with the OS and 3rd party software dependencies defined in the CHEOPS SOC reference platform and provides a quick and easy way to set up the development environment.

# Getting started

If this is your first time using VS Code and/or development containers, please follow the instructions in [Getting Started](https://aka.ms/vscode-remote/containers/getting-started) to install the necessary software on your computer.

1. git clone this repository to your computer: `git clone https://gitlab.unige.ch/cheops/containers/cheops-dev.git`. If you'd like to use a specific cheops-dev release, add the option `--branch <release>`, for example `--branch 14.0.0`. See [Container specifications](#container-specifications) for available releases.
2. Start VS Code and open the folder you'd like to use as your workspace, aka your project folder.
3. Move the `.devcontainer` folder that is located inside `cheops-dev/` in the cloned repository into the project folder.
4. Press F1 and choose **Remote-Containers: Reopen Folder in Container**.

Your project folder is now shared with the container and you are interacting with the container via VS Code.

See [Developing inside a Container](https://aka.ms/vscode-remote/containers/) for more details on the usage of development containers.

## Installation of SOC software

The SOC products can be installed inside the container using the `dnf` package manager in the VS Code terminal. In order for this to work, the `CHEOPS_SW` environment variable must point to the folder `/opt/cheops` inside the container. _Please note that earlier incarnations of `.devcontainer/devcontainer.json` would set `CHEOPS_SW` to a different path. If that's the case for you, comment out  or remove that part of `.devcontainer/devcontainer.json` and restart the container._

The SOC products are grouped into the following dnf packages.

| dnf package name    | SOC products in package |
|---------------------|-------------------------|
| **common_sw**       | common_sw               |
| **xml_schema**      | xml_schema              |
| **infrastructure**  | operations_tools <br> pre_archive_processing <br> reporting_tools <br> science_tm <br> tool_box |
| **preprocessing**   | combination <br> conversion |
| **data_reduction**  | dr_tools <br> image_reduction_calibration <br> image_reduction_correction <br> photometric_extraction |
| **quick_look**      | QuickLook <br> dark_monitoring <br> mco_modules |
| **monitor4cheops**  | monitor4cheops <br> monitor4cheops_config <br> processing_tools |

### A quick introduction to dnf
To install one or more packages, run

    sudo dnf install <package-names>

where `<package-names>` is replaced with the names of one or more packages. Example: `sudo dnf install common_sw xml_schema infrastructure`. To install specific versions of packages, append the version number to the package name, for example `common_sw-15.0.1`. If the command gives an error, try adding the `--refresh` option to force an update dnf's cache.

Similarly, to remove packages, run

    sudo dnf remove <package-names>

To list the packages that are available in the cheops repository, run `dnf list | grep cheops`. To list the versions of the SOC products included in a specific package, run `dnf info <package-name>` or `dnf info <package-name>-<package-version>`. Example: `dnf info infrastructure-15.0.1`.

For an overview of the cheops repository, see [Intel](http://repository.astro.unige.ch/cheops/almalinux/9/x86_64/repoview/)/[Apple chip](http://repository.astro.unige.ch/cheops/almalinux/9/aarch64/repoview/).

## Customizations

The file `.devcontainer/devcontainer.json` defines user specific customizations of the development container. These customizations will persist even if the container is deleted and recreated. Here are a few examples of what can be changed:
* To use a different release of cheops-dev, modify the release number in the `image` attribute to the version of the cheops-dev container you'd like to use.
* The `extensions` attribute defines VS Code extensions that are installed into the cheops-dev container, separate from the global VS Code extensions. Additional extensions can be added to this list.
* The `remoteEnv` attribute sets environment variables that will be available when using the container in VS Code. The commented out variables changes the location in which SOC software is installed, from the default `/opt/cheops` to the directory `build/` inside the VS Code workspace. See [Variables in devcontainer.json](https://containers.dev/implementors/json_reference/#variables-in-devcontainerjson) for an explanation of the special variables used in this example. Note that if the value of `CHEOPS_SW` is changed, SOC products cannot be installed using the dnf package manager.

See the [devcontainer.json specification](https://containers.dev/implementors/json_reference/) for a full overview of the possible attributes.

# Container specifications

> ⚠ WARNING:
> cheops-dev:14.0.0 cannot be used as a development container in Visual Studio Code release 1.86 and above. This is because Centos7 has become incompatible as a development container due to an increase of the minumum requirement for dev containers' build toolchain. See [Can I run VS Code Server on older Linux distributions?](https://code.visualstudio.com/docs/remote/faq#_can-i-run-vs-code-server-on-older-linux-distributions) for more information. 

Supported architectures:
* amd64/x86_64
* arm64/aarch64

| Software            | cheops-dev:15.0.0 | cheops-dev:14.0.0 |
|---------------------|-------------------|-------------------|
| **Container OS**    | Alma9             | Centos7           |
| **boost**           | 1.78.0            | 1.59.0            |
| **cfitsio**         | 4.1.0             | 3.370             |
| **doxygen**         | 1.10.0            | 1.8.12            |
| **gcc**             | 11.4.1            | 4.9.4             |
| **python**          | 3.9.18            | 3.6.8             |
| **- astropy**       | 6.0.1             | 3.0.5             |
| **- django**        | 4.2.13            | 1.2               |
| **- ipdb**          | 0.13.13           | 0.13.13           |
| **- Jinja2**        | 3.1.4             | 2.10              |
| **- lmfit**         | 1.3.1             | 0.9.11            |
| **- lxml**          | 5.2.2             | 4.9.3             |
| **- matplotlib**    | 3.8.4             | 3.0.1             |
| **- notebook**      | 6.4.13            | 6.4.10            |
| **- numpy**         | 1.26.4            | 1.18.1            |
| **- pandas**        | 2.2.2             | 1.0.2             |
| **- pypdf**         | 4.2.0             | N/A               |
| **- PyPdf2**        | N/A               | 1.26.0            |
| **- regions**       | 0.8               | 0.4               |
| **- scikit-image**  | 0.22.0            | 0.14.2            |
| **- scipy**         | 1.13.0            | 1.1.0             |
| **- seaborn**       | 0.13.2            | 0.11.2            |
| **- sphinx**        | 7.3.7             | 1.7.9             |
| **- sqlalchemy**    | 2.0.30            | 1.3.18            |
| **- statsmodels**   | 0.14.2            | 0.11.1            |
| **- tabulate**      | 0.9.0             | 0.8.2             |
| **svn**             | 1.14.1            | 1.7.14            |
| **texlive**         | 2020              | 2023              |
| **xerces-c**        | 3.2.5             | 3.1.1             |
| **xsd**             | 4.1.0             | 4.0.0             |

# Support 

For support, please contact the CHEOPS Science Operations Centre.

