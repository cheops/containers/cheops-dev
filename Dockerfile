from almalinux:9

# Arguments

ARG TARGETPLATFORM
ARG CONTAINER_NAME=cheops-dev
ARG BUILD_DATE

ARG USER_NAME=cheops
ARG USER_UID=1000
ARG USER_GID=$USER_UID
ARG USER_HOME=/home/$USER_NAME
ARG USER_BASHRC_DIR=$USER_HOME/.bashrc.d/
ARG USER_BASHRC=$USER_BASHRC_DIR/bashrc_cheops

ARG BOOST_VERSION=1.78

ARG DOXYGEN_VERSION=1.10.0
ARG DOXYGEN_PREFIX=/opt/doxygen/${DOXYGEN_VERSION}

ARG PY_VERSION=3
ARG PY_VENV_PREFIX=/opt/venv/cheops/python${PY_VERSION}

# Environment

#ENV JAVA_HOME=/etc/alternatives/java_sdk_1.8.0
#ENV JRE_HOME=/etc/alternatives/jre_1.8.0

ENV CHEOPS_SW=/opt/cheops
ENV PATH=$CHEOPS_SW/bin:${DOXYGEN_PREFIX}/bin:$PATH
ENV PYTHONPATH=$CHEOPS_SW/python:$CHEOPS_SW/lib
ENV LD_LIBRARY_PATH=$CHEOPS_SW/lib:/usr/local/lib:/usr/local/lib64:$LD_LIBRARY_PATH
ENV EXT_INC_DIRS="-I/usr/include/boost${BOOST_VERSION} -I/usr/include/cfitsio -I/usr/include/eigen3 -I/usr/local/include"
ENV EXT_LIB_DIRS="-L/usr/lib64/boost${BOOST_VERSION} -L/usr/local/lib -L/usr/local/lib64"
ENV PYTHONIOENCODING=UTF-8

# Image labels

LABEL cheops.image.build.date="${BUILD_DATE}"
LABEL cheops.image.maintainer="CHEOPS Science Operations Centre"

WORKDIR /tmp

SHELL ["/bin/bash", "-euxo", "pipefail", "-c"]

RUN \
    #
    # Root user
    # 
    echo "export PS1='\t [\u@$CONTAINER_NAME \W]# '" >> /root/.bashrc; \
    #
    # Cheops user
    #
    groupadd --gid $USER_GID $USER_NAME; \
    useradd --uid $USER_UID --gid $USER_GID --home-dir $USER_HOME --create-home --shell /bin/bash $USER_NAME; \
    mkdir -p $USER_BASHRC_DIR; \
    touch $USER_BASHRC; \
    chown -R $USER_NAME:$USER_NAME $USER_BASHRC_DIR; \
    echo "$USER_NAME ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers; \
    echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers; \
    echo "export PS1='\t [\u@$CONTAINER_NAME \W]# '" >> $USER_BASHRC; \
    echo "--.mode column" >> $USER_HOME/.sqliterc; \
    echo ".headers on" >> $USER_HOME/.sqliterc; \
    #
    # Default CHEOPS software installation area
    #
    mkdir -p $CHEOPS_SW; \
    mkdir -p $CHEOPS_SW/{bin,conf,doc,include,lib,python,resources}; \
    chown -R $USER_NAME:$USER_GID $CHEOPS_SW; \
    #
    # System repositories
    #
    dnf -y update; \
    dnf -y install epel-release; \
    dnf config-manager --enable epel; \
    dnf -y install yum-utils; \
    dnf config-manager --set-enabled crb; \
    #
    # System packages
    #
    dnf -y install \
        bzip2 \
        cfitsio \
        cfitsio-devel \
        dpkg \
        diffutils \
        eigen3 \
        ghostscript \
        gcc-c++ \
        graphviz \
        graphviz-devel \
        gsl \
        gsl-devel \
        #java-1.8.0-openjdk \
        #java-1.8.0-openjdk-devel \
        lcov \
        lzma \
        make \
        man \
        nano \ 
        openssl-devel \
        openssh \
        openssh-clients \
        pandoc \ 
        procps \
        rsync \
        sqlite \
        sqlite-devel \
        sudo \
        svn \
        tcsh \
        time \
        valgrind \
        vim \
        wget \
        which \
        xerces-c \
        xerces-c-devel \
        zstd; \
    dnf -y install --allowerasing coreutils; \
    # 
    # Cheops repository
    # 
    wget --no-verbose http://repository.astro.unige.ch/cheops/almalinux/yum/cheops.repo -P /etc/yum.repos.d/; \
    dnf config-manager --add-repo /etc/yum.repos.d/cheops.repo; \
    dnf config-manager --set-enabled cheops; \
    #
    # xsd
    #
    dnf -y install xsd; \
    ln -s /usr/bin/xsdcxx /usr/bin/xsd; \
    #
    # wkhtmltopdf
    #
    wkhtml_pkg=wkhtmltox-0.12.6.1-3.almalinux9.$(uname -m).rpm; \
    wget --no-verbose https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6.1-3/$wkhtml_pkg; \
    dnf -y localinstall $wkhtml_pkg; \
    rm -f $wkhtml_pkg; \
    #
    # Texlive
    #
    dnf -y install --nodocs texlive*; \
    dnf -y install latexmk; \
    # 
    # Doxygen
    # See https://www.doxygen.nl/manual/install.html
    # 
    dnf -y install bison cmake flex; \
    wget --no-verbose https://www.doxygen.nl/files/doxygen-${DOXYGEN_VERSION}.src.tar.gz; \
    tar -xzf doxygen-${DOXYGEN_VERSION}.src.tar.gz; \
    cd doxygen-${DOXYGEN_VERSION}; \
    mkdir build; \
    cd build; \
    cmake -DCMAKE_INSTALL_PREFIX=$DOXYGEN_PREFIX -G "Unix Makefiles" ..; \
    make; \
    make install; \
    cd ../../; \
    rm -rf doxygen-${DOXYGEN_VERSION}; \
    # 
    # Python
    # 
    dnf -y install \
        python${PY_VERSION} \
        python${PY_VERSION}-devel \
        python${PY_VERSION}-pip \
        python${PY_VERSION}-tkinter; \
    python${PY_VERSION} -m venv ${PY_VENV_PREFIX}; \
    # Prevent venv activation from changing the prompt
    echo "export VIRTUAL_ENV_DISABLE_PROMPT=1" >> $USER_BASHRC; \
    echo ". ${PY_VENV_PREFIX}/bin/activate" >> $USER_BASHRC; \
    # The venv does not contain python3-config, create the link manually
    ln -s /usr/bin/python${PY_VERSION}-config ${PY_VENV_PREFIX}/bin/python3-config; \
    . ${PY_VENV_PREFIX}/bin/activate; \
    python${PY_VERSION} -m pip install --upgrade pip; \
    pip3 install --no-cache-dir --upgrade setuptools wheel; \
    pip3 install --no-cache-dir \
        astropy~=6.0.1 \
        beautifulsoup4 \
        bitstring~=4.2.2 \
        coverage~=7.5.1 \
        Django~=4.2.13 \
        django-extensions~=3.2.3 \
        django-filter~=24.2 \
        gcovr~=7.2 \
        ipdb~=0.13.13 \
        Jinja2~=3.1.4 \
        jupyter-contrib-nbextensions~=0.7.0 \
        lmfit~=1.3.1 \
        lxml~=5.2.2 \
        matplotlib~=3.8.4 \
        mypy~=1.10.0 \
        nbconvert~=7.16.4 \
        # notebook 7 and above does not support the required plugins \
        notebook~=6.4.0 \
        numpy~=1.26.4 \
        pandas~=2.2.2 \
        pandoc~=2.3 \
        plotly~=5.22.0 \
        pyarrow~=16.0.0 \
        pygraphviz~=1.11 \
        pylint~=3.1.1 \
        pypdf~=4.2.0 \
        pytest~=8.2.0 \
        PyYAML~=6.0.1 \
        regions~=0.8 \
        scikit-image~=0.22.0 \
        scipy~=1.13.0 \
        seaborn~=0.13.2 \
        Sphinx~=7.3.7 \
        #sqlacodegen # currently requires sqlalchemy < 2.0 \
        SQLAlchemy~=2.0.30 \
        statsmodels~=0.14.2 \
        tabulate~=0.9.0; \
    jupyter contrib nbextension install --sys-prefix; \
    jupyter nbextension enable python-markdown/main; \
    # Deactivate the virtual python environment
    deactivate; \
    # 
    # Boost
    # 
    dnf -y install boost${BOOST_VERSION}*; \
    # 
    # CHEOPSim dependencies
    # 
    # Note: ant and tomcat installs java 1.11 as dependency
    dnf -y install \
        ant \
        ffmpeg-free \
        ffmpeg-free-devel \
        git \
        libpq \
        libpq-devel \
        postgresql \
        tomcat; \
    git clone https://github.com/jtv/libpqxx.git; \
    cd libpqxx/; \
    git checkout 6.4; \
    # The config/config.guess file in the git repo is too old and gives the 
    # error "configure: error: cannot guess build type; you must specify one".
    # The file system contains a more recent version of the file, use this one
    # instead.
    cp -p /usr/share/automake-1.16/config.guess config/config.guess; \
    ./configure --enable-shared --disable-documentation --prefix /usr; \
    make; \
    make install; \
    cd ..; \
    rm -rf libpqxx/; \
    #
    # Clean dnf
    #
    dnf clean all; \
    rm -rf /var/cache/dnf/*; 

# Port 8888: jupyter notebook
# Port 8000: django development server
EXPOSE 8888 8000

USER $USER_NAME

WORKDIR $USER_HOME
